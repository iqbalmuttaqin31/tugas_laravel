<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function toRegister(){
        return view('Register');
        }

        
    public function toWelcome(Request $request){

        $namaDepan = $request['firstname'];
        $namaBelakang = $request['lastname'];
        return view('Welcome', compact('namaDepan','namaBelakang'));
        }

   
}

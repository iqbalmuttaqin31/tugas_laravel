<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pembuatan Akun Baru</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <!-- Tag awal pembuatan form -->
    <form action="/Welcome" method="POST">
    @csrf
        <!-- Tag membuat inputan untuk data nama -->
        <label>First name:</label> <br><br>
        <input type="text" name="firstname"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="lastname"> <br><br>

        <!-- Tag membuat radio button untuk input data pemilihan jenis kelamin -->
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>

        <!-- Tag membuat dropdown untuk input data pemilihan negara asal -->
        <label>Nationality:</label> <br><br>
        <select name="region">
            <option value="Brunai">Brunai Darussalam</option>
            <option value="Filipina">Filipina</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Kamboja">Kamboja</option>
            <option value="Laos">Laos</option>
            <option value="Myanmar">Myanmar</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
            <option value="Thailand">Thailand</option>
            <option value="Timor Leste">Timor Leste</option>
            <option value="Vietnam">Vietnam</option>
        </select> <br><br>

        <!-- Tag membuat pilihan ganda/checkbox untuk input data pemilihan penggunaan bahasa -->
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>

        <!-- Tag membuat textarea untuk input data bio -->
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>

        <!-- Tag membuat tombol untuk submit form -->
        <input type="submit" value="Sign Up"></input>

    </form>
    <!-- Tag akhir pembuatan form -->

</body>

</html>